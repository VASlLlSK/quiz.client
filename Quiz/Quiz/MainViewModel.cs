﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using EasyHttp.Http;

namespace Quiz
{
    public class NPC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
    class MainViewModel : NPC
    {
        public ObservableCollection<Category> Categories { get; set; }
        public ObservableCollection<User> RatingCollection { get; set; }

        public Stack<DockPanel> Page;
        public CurrentUser currentUser { get; set; }
        Question ques;
        public Question question { get { return ques; } set { ques = value; OnPropertyChanged(); } }
        bool buttonsleep;

        Answer answerA;
        Answer answerB;
        Answer answerC;
        Answer answerD;
        public Answer AnswerA { get { return answerA; } set { answerA = value; OnPropertyChanged(); } }
        public Answer AnswerB { get { return answerB; } set { answerB = value; OnPropertyChanged(); } }
        public Answer AnswerC { get { return answerC; } set { answerC = value; OnPropertyChanged(); } }
        public Answer AnswerD { get { return answerD; } set { answerD = value; OnPropertyChanged(); } }



        MainWindow mainWindow;

        public MainViewModel(MainWindow MW)
        {
            Categories = new ObservableCollection<Category>();
            RatingCollection = new ObservableCollection<User>();
            Page = new Stack<DockPanel>();
            currentUser = CurrentUser.getInstance();
            question = new Question();
            mainWindow = MW;
        }

        private RelayCommand _previousPage;
        public RelayCommand PreviousPage
        {
            get
            {
                return _previousPage ??
                  (_previousPage = new RelayCommand(x =>
                  {
                      if (Page.Count > 1)
                      {
                          var someDockPanel = Page.Pop();
                          CollapsedSomeDockPanel(someDockPanel);
                          someDockPanel = Page.Peek();
                          VisibleSomeDockPanel(someDockPanel);
                      }
                  }));
            }
        }

        private RelayCommand _openRating;
        public RelayCommand OpenRating
        {
            get
            {
                return _openRating ??
                  (_openRating = new RelayCommand(x =>
                  {
                      var ui = x as MainWindow;
                      RatingCollection.Clear();

                      var client = new HttpClient();
                      var response = client.Get("https://localhost:44307/api/RatingTop10");
                      var UsersFromServer = response.StaticBody<User[]>();

                      int i = 1;
                      foreach (var user in UsersFromServer)
                      {
                          user.place = i;
                          RatingCollection.Add(user);
                          i++;
                      }

                      if (Page.Peek() != ui.rating)
                      {
                          var someDockPanel = Page.Peek();
                          CollapsedSomeDockPanel(someDockPanel);
                          VisibleSomeDockPanel(ui.rating);
                          Page.Push(ui.rating);
                      }
                  }));
            }
        }

        public void CollapsedAll(MainWindow ui)
        {
            ui.categories.Visibility = Visibility.Collapsed;
            ui.LK.Visibility = Visibility.Collapsed;
            ui.question.Visibility = Visibility.Collapsed;
            ui.rating.Visibility = Visibility.Collapsed;
        }
        public void VisibleSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Visible;
        }
        public void CollapsedSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Collapsed;
        }
        public void prepareAnswer()
        {
            AnswerA = new Answer(question.AnswerA);
            AnswerB = new Answer(question.AnswerB);
            AnswerC = new Answer(question.AnswerC);
            AnswerD = new Answer(question.AnswerD);
        }

        private RelayCommand _sendName;
        public RelayCommand SendName
        {
            get
            {
                return _sendName ??
                  (_sendName = new RelayCommand(x =>
                  {
                      var ui = x as MainWindow;
                      var client = new HttpClient();

                      string url = "https://localhost:44307/api/User";
                       
                      string data = "\"" + ui.userName.Text.Trim() + "\"";

                      var response = client.Post(url, data, HttpContentTypes.ApplicationJson);
                      bool AnswerfromServer = response.StaticBody<bool>();

                      if (AnswerfromServer == true || currentUser.name == ui.userName.Text.Trim())
                      {
                          response = client.Get("https://localhost:44307/api/category");
                          var categoriesFromServer = response.StaticBody<Category[]>();
                          foreach (var category in categoriesFromServer)
                              Categories.Add(category);

                          var someDockPanel = Page.Peek();
                          CollapsedSomeDockPanel(someDockPanel);
                          VisibleSomeDockPanel(ui.categories);
                          Page.Push(ui.categories);

                          if (currentUser.name != ui.userName.Text)
                              currentUser.Score = 0;

                          currentUser.name = ui.userName.Text.Trim();
                          
                      }
                      else
                      {
                          ui.userName.BorderThickness = new Thickness(3);
                          ui.userName.BorderBrush = Brushes.Red;
                      }
                  },
                    x => {
                        var ui = x as MainWindow;
                        if (ui.userName.Text.Trim() == "")
                            return false;
                        return true;
                    })
);
            }
        }

        private RelayCommand _categoryButton;
        public RelayCommand CategoryButton
        {
            get
            {
                return _categoryButton ??
                  (_categoryButton = new RelayCommand(x =>
                  {
                      var ui = x as Category;
                      var client = new HttpClient();

                      string url = "https://localhost:44307/api/Question/" + ui.category;
                      string data = "\"" + CurrentUser.getInstance().name + "\"";
                      var response = client.Post(url, data, HttpContentTypes.ApplicationJson);

                      question = response.StaticBody<Question>();
                      if (question != null)
                      {
                          var customer = response.DynamicBody;
                          question.AnswerA = customer.AnswerA;
                          question.AnswerB = customer.AnswerB;
                          question.AnswerC = customer.AnswerC;
                          question.AnswerD = customer.AnswerD;
                          prepareAnswer();
                      }
                      else
                      {
                          question = new Question();
                          question.question = "Вопросы закончились! перейдите в след. категорию!";
                          question.AnswerA = "";
                          question.AnswerB = "";
                          question.AnswerC = "";
                          question.AnswerD = "";
                          prepareAnswer();
                      }

                      var someDockPanel = Page.Peek();
                      CollapsedSomeDockPanel(someDockPanel);
                      VisibleSomeDockPanel(mainWindow.question);
                      Page.Push(mainWindow.question);
                      prepareAnswer();

                  }));
            }
        }


        private RelayCommand _answerButton;
        public RelayCommand AnswerButton
        {
            get
            {
                return _answerButton ??
                  (_answerButton = new RelayCommand(x =>
                  {
                      var ui = x as Button;
                      var client = new HttpClient();

                      if (ui.Content.ToString() != "" && !buttonsleep)
                      {
                          buttonsleep = true;
                          string url = "https://localhost:44307/api/Question/" + question.id + "/" + ui.Content;
                          string data = "\"" + CurrentUser.getInstance().name + "\"";
                          var response = client.Put(url, data, HttpContentTypes.ApplicationJson);
                          bool b = response.StaticBody<bool>();


                          ThicknessAnimation buttonAnimation = new ThicknessAnimation();
                          buttonAnimation.From = ui.BorderThickness;
                          buttonAnimation.By = new Thickness(4);
                          buttonAnimation.Duration = TimeSpan.FromSeconds(0.5);
                          buttonAnimation.RepeatBehavior = new RepeatBehavior(4);
                          buttonAnimation.AutoReverse = true;
                          if (b)
                          {
                              ui.BorderBrush = Brushes.Green;
                              CurrentUser.getInstance().Score++;
                          }
                          else
                          {
                              ui.BorderBrush = Brushes.Red;
                              CurrentUser.getInstance().Score--;
                          }
                          ui.BeginAnimation(Button.BorderThicknessProperty, buttonAnimation);



                          Task.Run(() =>
                          {
                              Thread.Sleep(4000);

                              url = "https://localhost:44307/api/Question/" + question.category;
                              data = "\"" + CurrentUser.getInstance().name + "\"";

                              response = client.Post(url, data, HttpContentTypes.ApplicationJson);

                              question = response.StaticBody<Question>();
                              if (question != null)
                              {
                                  var customer = response.DynamicBody;
                                  question.AnswerA = customer.AnswerA;
                                  question.AnswerB = customer.AnswerB;
                                  question.AnswerC = customer.AnswerC;
                                  question.AnswerD = customer.AnswerD;
                              }
                              else
                              {
                                  question = new Question();
                                  question.question = "Вопросы закончились! перейдите в след. категорию!";
                                  question.AnswerA = "";
                                  question.AnswerB = "";
                                  question.AnswerC = "";
                                  question.AnswerD = "";
                              }
                              prepareAnswer();
                              buttonsleep = false;
                          });

                      }
                  }));
            }
        }


    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
    public class Answer : NPC
    {
        string _answer;
        public string answer { get { return _answer; } set { _answer = value; OnPropertyChanged(); } }

        public Answer(string a)
        {
            answer = a;
        }
    }

    public class User
    {
        public int place { get; set; }
        public string name { get; set; }
        public int score { get; set; }
    }
    public class Category
    {
        public int id { get; set; }
        public string category { get; set; }
        public string categoryName { get; set; }

    }


    public class Question : NPC
    {
        public int id { get; set; }
        public string category { get; set; }
        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }

        string _question;
        public string question { get { return _question; } set { _question = value; OnPropertyChanged(); } }

    }
    public class CurrentUser : NPC
    {
        private static CurrentUser instance;
        public string name { get; set; }
        private int _score { get; set; }
        public int Score { get { return _score; } set { _score = value; OnPropertyChanged(); } }
        private CurrentUser()
        {
            _score = 0;
            name = "Rick";
        }

        public static CurrentUser getInstance()
        {
            if (instance == null)
                instance = new CurrentUser();
            return instance;
        }

    }
}
